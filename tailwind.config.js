const range = require('lodash.range');

module.exports = {
  theme: {
    screens: {
      's': '640px',
      'm': '768px',
      'l': '1024px',
      'xl': '1280px',
    },
  },
  variants: ['responsive'],
  plugins: [
    require('./plugins/grid')({
      gridGutters: {
        's': '1',
        'm': '2',
        'l': '3',
        'xl': '4',
      },
    }),
  ],
}