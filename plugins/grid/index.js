const range = require('lodash.range');
const max = require('lodash.max');

/**
 * 
 * @see https://tailwindcss.com/docs/plugins/
 * @param {array} columnCount
 * @param {object} gridGutters
 * @param {string} gridGutterUnit
 * @param {array} variants
 */
module.exports = function ({ columnCount = range(1, 12), gridGutters, gridGutterUnit = 'rem', variants = ['responsive'] }) {
  return function ({ addUtilities, theme, e }) {
    /**
     * Add Utility without variants
     */
    addUtilities([
      /**
       * Add per breakpoint rules
       * using matching keys for theme screens & gridGutters
       */
      ...Object.keys(theme('screens')).map(breakpoint => ({
        // Add padding's to .grid
        [`.${e(`${breakpoint}:grid`)}`]: {
          paddingRight: (gridGutters[breakpoint] * 0.5) + gridGutterUnit,
          paddingLeft: (gridGutters[breakpoint] * 0.5) + gridGutterUnit,
        },
        // Add margin's to .grid-row
        [`.${e(`${breakpoint}:grid-row`)}`]: {
          marginRight: (gridGutters[breakpoint] * -0.5) + gridGutterUnit,
          marginLeft: (gridGutters[breakpoint] * -0.5) + gridGutterUnit,
          // Add padding's to .grid-col-
        },
        [`.${e(`[class*=${breakpoint}:'grid-col-']`)}`]: {
          paddingRight: (gridGutters[breakpoint] * 0.5) + gridGutterUnit,
          paddingLeft: (gridGutters[breakpoint] * 0.5) + gridGutterUnit,
        },
      })),
    ]),
    /**
     * Add grid utility with responsive variants
     */
    addUtilities([
      { "[class*='grid-col-'].grid-col-center": { minHeight: '1px' } },
      /**
       * Grid
       */ 
      { '.grid': {
        minHeight: '1px',
        marginRight: 'auto',
        marginLeft: 'auto',
      } },
      /**
       * No gutter / nested grids
       */
      { '.grid-no-gutter, .grid-nested': { 
        paddingRight: '0',
        paddingLeft: '0',
      } },
      { '.grid-no-gutter > .grid-row': { 
        marginRight: '0',
        marginLeft: '0',
      } },
      /**
       * Grid row
       */
      { '.grid-row': { 
        position: 'relative',
        '&:after': {
          content: e("''"),
          display: 'block',
          clear: 'both',
        }
      } },
      /**
       * Griw row flex
       */
      { '.grid-row-end, .grid-row-center, .grid-row-stretch': { 
        display: 'flex',
        flexWrap: 'wrap',
      } },
      { '.grid-row-end .grid-col-left, .grid-row-center .grid-col-left, .grid-row-stretch .grid-col-left': { 
        order: '0',
      } },
      { '.grid-row-end .grid-col-right, .grid-row-center .grid-col-right, .grid-row-stretch .grid-col-right': { 
        order: '1',
      } },
      /**
       * Row stretch
       */
      { '.grid-row-stretch': { alignItems: 'stretch' } },
      /**
       * Row center
       */
      { '.grid-row-center': { alignItems: 'center' } },
      /**
       * Row start
       */
      { '.grid-row-start': { alignItems: 'flex-start' } },
      /**
       * Row end
       */
      { '.grid-row-end': { alignItems: 'flex-end' } },
      /**
       * Row stretch
       */
      { '.grid-row-stretch > [class*=grid-col-]': { 
        display: 'flex',
        alignItems: 'stretch',
       } },
      /**
       * Column center
       */
      { "[class*='grid-col-'].grid-col-center": { 
        float: 'none',
        marginRight: 'auto',
        marginLeft: 'auto',
        clear: 'both',
      } },
      /**
       * Clear
       */ 
      { "[class*='grid-col-'].grid-col-clear": { clear: 'both' } },
      { "[class*='grid-col-'].grid-col-no-clear": { float: 'none' } },
      /**
       * Create zero based grid classes
       */
      { '.grid-pull-0': { marginLeft: '0' } },
      { '.grid-push-0': { marginLeft: '0' } },
      { '.grid-col-0': { display: 'none' } },
      /**
       * Create grid-pull-n
       * margin-left : -33.3333%
       */ 
      ...range(1, max(columnCount) + 2).map((count) => ({
        [`.grid-pull-${count}`]: {
          marginLeft: `${(count) * -100 / (max(columnCount) + 1)}%`,
        },
      })),
      /**
       * Create grid-push-n
       * margin-left : 33.3333%
       */ 
      ...range(1, max(columnCount) + 2).map((count) => ({
        [`.grid-push-${count}`]: {
          marginLeft: `${(count) * 100 / (max(columnCount) + 1)}%`,
        },
      })),
      /**
       * Create grid-col-n
       * width: 100%
       */ 
      ...range(1, max(columnCount) + 2).map((count) => ({
        [`.grid-col-${count}`]: {
          float: 'left',
          display: 'block',
          width: `${(count) * 100 / (max(columnCount) + 1)}%`,
        },
      })),
      /**
       * Floating
       */
      { "[class*='grid-col-'].grid-col-right": { float: 'right' } },
      { "[class*='grid-col-'].grid-col-left": { float: 'left' } },
    ], variants)
  }
}