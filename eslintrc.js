module.exports = {
  extends: '@studiometa/eslint-config/base',
  env: { 
    'es6': true,
  },
  rules: {
    'func-names': ['error', 'never'],
    'max-len': ['error', {
      'code': 120,
    }],
  }
};